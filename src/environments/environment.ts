// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase : {
    apiKey: "AIzaSyD1nTeaMMgYLbtwkx-567LgVow8Rl4MUyQ",
    authDomain: "chess-c01de.firebaseapp.com",
    databaseURL: "https://chess-c01de.firebaseio.com",
    projectId: "chess-c01de",
    storageBucket: "chess-c01de.appspot.com",
    messagingSenderId: "543691358264",
    appId: "1:543691358264:web:d5d236a8bdb20e5ef926cd"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
